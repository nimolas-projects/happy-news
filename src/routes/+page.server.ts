import { extract, type FeedData, type FeedEntry } from '@extractus/feed-extractor'

export interface OrderedRSSItem extends FeedEntry {
    badWordCount: number
}

const parseRSSFeed = (url: string): Promise<FeedData> => extract(url)


const sortByHappy = (entries: FeedEntry[]) => {
    const wordFilter = ["cancer", "bad", "war", "genocide", "murder", "death", "kill", "brexit", "tories", "conservatives", "die",   "tory"]

    return entries.map(entry => {
        const description = (entry.description as string).toLowerCase()
        const newEntry: OrderedRSSItem = { ...entry, badWordCount: 0 }

        wordFilter.forEach(word => description.includes(word) ? newEntry.badWordCount += 1 : newEntry.badWordCount += 0)
        return newEntry
    }).sort((a, b) => a.badWordCount > b.badWordCount ? 1 : -1)
}

export async function load() {
    const guardianData: OrderedRSSItem[] = await parseRSSFeed("https://www.theguardian.com/world/rss").then(response => sortByHappy(response.entries as FeedEntry[]))
    return { guardianData }
}